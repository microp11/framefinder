﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrameFinder
{
    public partial class MainForm : Form
    {
        private CombinedFinder partialFinder;
        private long count;

        public MainForm()
        {
            InitializeComponent();

            partialFinder = new CombinedFinder();
            partialFinder.OnFrameDetected += Finder_FrameDetected;
        }

        private void Finder_FrameDetected(object sender, FrameArgs e)
        {
            if (e.UnmatchedSymbols > 0)
            {
                richTextBox1.AppendText(string.Format("-----------------------> Unmatched symbols: {0}", e.UnmatchedSymbols));
                richTextBox1.AppendText(Environment.NewLine);
            }

            if (e.IsNormalPolarity)
            {
                richTextBox1.AppendText(string.Format("Normal frame: {0} - {1}, normal errors: {2}, inverted errors {3}", e.StartPosition, e.StartPosition + FinderConsts.FrameLength - 1, e.NormalUWErrors, e.InvertedUWErrors));
            }
            else
            {
                richTextBox1.AppendText(string.Format("Inverted frame: {0} - {1}, normal errors: {2}, inverted errors {3}", e.StartPosition, e.StartPosition + FinderConsts.FrameLength - 1, e.NormalUWErrors, e.InvertedUWErrors));
            }
            richTextBox1.AppendText(Environment.NewLine);
        }

        private void btnSource_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }
        }

        /// <summary>
        /// Cannot be cancelled as is.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button1_Click_1(object sender, EventArgs e)
        {
            count = 0;
            partialFinder.Tolerance= (int)numericUpDown1.Value;
            partialFinder.Reset();

            try
            {
                using (FileStream sourceStream = File.Open(textBox1.Text, FileMode.Open))
                {
                    await Detect(sourceStream, count);
                }
                Debug.WriteLine("finished {0}", count);
            }
            catch (Exception ex)
            {
                richTextBox1.AppendText(Environment.NewLine);
                richTextBox1.AppendText(ex.Message);
                richTextBox1.AppendText(Environment.NewLine);
            }
        }

        private async Task Detect(FileStream sourceStream, long count)
        {
            try
            {
                byte[] symbols = new byte[10000];
                int symbolsRead;
                while ((symbolsRead = await sourceStream.ReadAsync(symbols, 0, 10000)) != 0)
                {
                    count += symbolsRead;
                    //Debug.WriteLine("Read {0}", count);
                    partialFinder.Detect(symbols, symbolsRead);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}

﻿/*
 * microp11 2019
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * This code uses 4 partial finders. The number was selected based on
 * the given 64 UW table.
 * 
 * Based on 16 UW the maximum admisible tolerance is 1 per part. 
 * 
 */
 
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FrameFinder
{
    public class CombinedFinder : IFinder
    {
        private readonly byte[] register;
        private SymbolFiller symbolFiller;

        private readonly PartialFinder[] partialFinders;
        private List<PartialFinderArgs> detectedParts, prevDetectedParts;
        private long lastPosition;
        private int nUWErrors;
        private int iUWErrors;
        private int lastFinderId;
        private readonly int partialFrameLength;
        private readonly Stopwatch sw;

        public int Tolerance
        {
            get
            {
                return 4;
            }
            set
            {
                //always set to 1 otherwise we'll get false positives
                for (int i = 0; i < 4; i++)
                {
                    if (partialFinders[i] == null)
                        return;
                    partialFinders[i].Tolerance = 1;
                }
            }
        }

        public CombinedFinder()
        {
            sw = new Stopwatch();

            partialFrameLength = FinderConsts.FrameLength / 4;
            register = new byte[FinderConsts.FrameLength];
            symbolFiller = new SymbolFiller(partialFrameLength);
            detectedParts = new List<PartialFinderArgs>();
            prevDetectedParts = new List<PartialFinderArgs>();
            partialFinders = new PartialFinder[4];
            for (int i = 0; i < 4; i++)
            {
                partialFinders[i] = new PartialFinder(
                    i,
                    i * partialFrameLength,
                    FinderConsts.Pattern.Skip(i * 16).Take(16).ToArray(),
                    FinderConsts.PatternInverted.Skip(i * 16).Take(16).ToArray(),
                    partialFrameLength);
                partialFinders[i].OnPartialFind += CombinedFinder_OnPartialFind;
            }
            Tolerance = 4;
            lastFinderId = -1;
        }

        private void CombinedFinder_OnPartialFind(object sender, PartialFinderArgs e)
        {
            sw.Restart();

            //fix polarity
            if (!e.IsNormalPolarity)
            {
                e.PartialRegister = Invert(e.PartialRegister);
            }

            symbolFiller.SetSymbol(e.FinderId + 25);

            //TODO change logic to include as full frame a part of 4. No point in waiting for the next part
            //that may never arrive.
            //at this point last received symbol in "e" is equal with last received symbol in symbolFiller
            if (e.FinderId > lastFinderId)
            {
                detectedParts.Add(e);
            }
            else
            {
                //at this point we have detected the start of a new frame
                //the last received symbol in "e" is equal with last received symbol in symbolFiller
                //however we will need parts before to fill in the gaps.
                ComposeFrame();

                //start a new frame
                prevDetectedParts.Clear();
                prevDetectedParts.AddRange(detectedParts);
                detectedParts.Clear();

                detectedParts.Add(e);
            }

            Debug.WriteLine("Part {0}, {1}, start {2}, end {3}, Nrm {4}, Inv {5}, ticks {6}", e.FinderId, e.IsNormalPolarity, e.FirstSymbol, e.LastSymbol, e.NormalUWErrors, e.InvertedUWErrors, sw.ElapsedTicks);
            lastFinderId = e.FinderId;
            lastPosition = e.LastSymbol;
        }

        private void ComposeFrame()
        {
            //the detected parts can start at any point in the stream
            //the detected parts have Ids between 0 to 3 inclusive
            //the detected parts are not always being received consecutive or contiguous
            long fromA, fromB, toA, toB;

            //encode the parts into a byte, right-side nibble, left nibble always 0xF
            //every missing part is a zero, rest is 1
            int parts = 0xFF;

            int trueDetectedParts;

            if (detectedParts.Count != 4)
            {
                ///1 part is missing
                ///
                /// 012-                    = 0xFE
                /// 01-3                    = 0xFD
                /// 0-23                    = 0xFB
                /// -123                    = 0xF7

                ///2 parts are missing
                /// 01--                    = 0xFC 
                /// 0-2-                    = 0xFA
                /// 0--3                    = 0xF9
                /// -12-                    = 0xF6
                /// -1-3                    = 0xF5
                /// --23                    = 0xF3

                ///3 parts are missing
                ///
                /// 0---                    = 0xF8
                /// -1--                    = 0xF4
                /// --2-                    = 0xF2
                /// ---3                    = 0xF1

                if (!detectedParts.Any(a => a.FinderId == 0))
                {
                    parts = parts & 0xF7;
                }

                if (!detectedParts.Any(a => a.FinderId == 1))
                {
                    parts = parts & 0xFB;
                }

                if (!detectedParts.Any(a => a.FinderId == 2))
                {
                    parts = parts & 0xFD;
                }

                if (!detectedParts.Any(a => a.FinderId == 3))
                {
                    parts = parts & 0xFE;
                }
            }

            trueDetectedParts = detectedParts.Count;

            switch (parts)
            {
                //no missing parts, added for brevity
                case 0xFF:
                    break;
                    
                // zero index, one missing part
                case 0xFE:
                    //1110 OK
                    //ideal position   : 0123
                    //detectedParts ids: 012
                    fromA = detectedParts[2].LastSymbol + 1;
                    toA = detectedParts[2].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 3 missing from {0} to {1}.", fromA, toA);
                    detectedParts.Add(symbolFiller.GetPart(3, fromA, toA));
                    break;

                case 0xFD:
                    //1101 OK
                    //ideal position   : 0123
                    //detectedParts ids: 013
                    fromA = detectedParts[1].LastSymbol + 1;
                    toA = detectedParts[1].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 2 missing from {0} to {1}.", fromA, toA);
                    detectedParts.Insert(2, symbolFiller.GetPart(2, fromA, toA));
                    break;

                case 0xFB:
                    //1011 OK
                    //ideal position   : 0123
                    //detectedParts ids: 023
                    fromA = detectedParts[0].LastSymbol + 1;
                    toA = detectedParts[0].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 1 missing from {0} to {1}.", fromA, toA);
                    detectedParts.Insert(1, symbolFiller.GetPart(1, fromA, toA));
                    break;

                case 0xF7:
                    //0111 OK
                    //ideal position   : 0123
                    //detectedParts ids: 123
                    fromA = detectedParts[0].FirstSymbol - 1 - partialFrameLength;
                    toA = detectedParts[0].FirstSymbol - 1;
                    Debug.WriteLine("Part 0 missing from {0} to {1}.", fromA, toA);
                    detectedParts.Insert(0, symbolFiller.GetPart(0, fromA, toA));
                    break;

                // zero index, two missing parts
                case 0xFC:
                    //1100 OK
                    //ideal position   : 0123
                    //detectedParts ids: 01
                    fromA = detectedParts[1].LastSymbol + 1;
                    toA = detectedParts[1].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 2 missing from {0} to {1}.", fromA, toA);

                    fromB = detectedParts[1].LastSymbol + 1 + partialFrameLength;
                    toB = detectedParts[1].LastSymbol + 2 * partialFrameLength;
                    Debug.WriteLine("Part 3 missing from {0} to {1}.", fromB, toB);

                    detectedParts.Add(symbolFiller.GetPart(2, fromA, toA));
                    detectedParts.Add(symbolFiller.GetPart(3, fromB, toB));
                    break;

                case 0xFA:
                    //1010 OK
                    //ideal position   : 0123
                    //detectedParts ids: 02
                    fromA = detectedParts[0].LastSymbol + 1;
                    toA = detectedParts[0].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 1 missing from {0} to {1}.", fromA, toA);

                    fromB = detectedParts[1].LastSymbol + 1;
                    toB = detectedParts[1].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 3 missing from {0} to {1}.", fromB, toB);

                    detectedParts.Insert(1, symbolFiller.GetPart(1, fromA, toA));
                    detectedParts.Add(symbolFiller.GetPart(3, fromB, toB));
                    break;

                case 0xF9:
                    //1001 OK
                    //ideal position   : 0123
                    //detectedParts ids: 03
                    fromA = detectedParts[0].LastSymbol + 1;
                    toA = detectedParts[0].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 1 missing from {0} to {1}.", fromA, toA);

                    fromB = detectedParts[0].LastSymbol + 1 + partialFrameLength;
                    toB = detectedParts[0].LastSymbol + 2 * partialFrameLength;
                    Debug.WriteLine("Part 2 missing from {0} to {1}.", fromB, toB);

                    detectedParts.Insert(1, symbolFiller.GetPart(1, fromA, toA));
                    detectedParts.Insert(2, symbolFiller.GetPart(2, fromB, toB));
                    break;

                case 0xF6:
                    //0110 OK
                    //ideal position   : 0123
                    //detectedParts ids: 12
                    fromA = detectedParts[0].FirstSymbol - 1 - partialFrameLength;
                    toA = detectedParts[0].FirstSymbol - 1;
                    Debug.WriteLine("Part 0 missing from {0} to {1}.", fromA, toA);

                    fromB = detectedParts[1].LastSymbol + 1;
                    toB = detectedParts[1].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 3 missing from {0} to {1}.", fromB, toB);

                    detectedParts.Insert(0, symbolFiller.GetPart(0, fromA, toA));
                    detectedParts.Add(symbolFiller.GetPart(3, fromB, toB));
                    break;

                case 0xF5:
                    //0101 OK
                    //ideal position   : 0123
                    //detectedParts ids: 13
                    fromA = detectedParts[0].FirstSymbol - 1 - partialFrameLength;
                    toA = detectedParts[0].FirstSymbol - 1;
                    Debug.WriteLine("Part 0 missing from {0} to {1}.", fromA, toA);

                    fromB = detectedParts[0].LastSymbol + 1;
                    toB = detectedParts[0].LastSymbol + partialFrameLength;
                    Debug.WriteLine("Part 2 missing from {0} to {1}.", fromB, toB);

                    detectedParts.Insert(0, symbolFiller.GetPart(0, fromA, toA));
                    detectedParts.Insert(2, symbolFiller.GetPart(2, fromB, toB));
                    break;

                case 0xF3:
                    //0011 OK
                    //ideal position   : 0123
                    //detectedParts ids: 23
                    fromA = detectedParts[0].FirstSymbol - 2 - 2 * partialFrameLength;
                    toA = detectedParts[0].FirstSymbol - partialFrameLength - 2;
                    Debug.WriteLine("Part 0 missing from {0} to {1}.", fromA, toA);

                    fromB = detectedParts[0].FirstSymbol - 1 - partialFrameLength;
                    toB = detectedParts[0].FirstSymbol - 1;
                    Debug.WriteLine("Part 1 missing from {0} to {1}.", fromB, toB);

                    symbolFiller.GetPart(1, fromB, toB);
                    symbolFiller.GetPart(0, fromA, toA);

                    detectedParts.Insert(0, symbolFiller.GetPart(0, fromA, toA));
                    detectedParts.Insert(1, symbolFiller.GetPart(1, fromB, toB));
                    break;

                // zero index, three missing parts, we do not decode
                default:
                    break;
            }

            //Debug.WriteLine("Frame had {0} detectedParts.", trueDetectedParts);
            //Debug.WriteLine("");

            if (detectedParts.Count == 4)
            {
                symbolFiller.GetPart(0, detectedParts[0].FirstSymbol, detectedParts[0].LastSymbol, true);
                symbolFiller.GetPart(1, detectedParts[1].FirstSymbol, detectedParts[1].LastSymbol, true);
                symbolFiller.GetPart(2, detectedParts[2].FirstSymbol, detectedParts[2].LastSymbol, true);
                symbolFiller.GetPart(3, detectedParts[3].FirstSymbol, detectedParts[3].LastSymbol, true);

                for (int i = 0; i < 4; i++)
                {
                    //write parts to frame register
                    Array.Copy(detectedParts[i].PartialRegister, 0, register, i * partialFrameLength, partialFrameLength);
                }

                FrameArgs args = new FrameArgs
                {
                    UnmatchedSymbols = 0,
                    StartPosition = detectedParts[0].FirstSymbol,
                    //EndPosition = e.NextFrameStartPosition,
                    NormalUWErrors = detectedParts[0].NormalUWErrors + detectedParts[1].NormalUWErrors + detectedParts[2].NormalUWErrors + detectedParts[3].NormalUWErrors,
                    InvertedUWErrors = detectedParts[0].InvertedUWErrors + detectedParts[1].InvertedUWErrors + detectedParts[2].InvertedUWErrors + detectedParts[3].InvertedUWErrors,
                    IsNormalPolarity = true,
                    IsElastic = false
                };
                OnFrameDetected?.Invoke(this, args);
            }

            Debug.WriteLine("Frame had {0} detectedParts.", trueDetectedParts);
            Debug.WriteLine("");
        }

        public event EventHandler<FrameArgs> OnFrameDetected;

        public void Detect(byte[] symbols, int count)
        {
            for (int i = 0; i < count; i++)
            {
                //add symbol to symbolFiller
                symbolFiller.Store(symbols[i]);
                
                //send same symbol to each detector, only one will detect
                for (int j = 0; j < 4; j++)
                {
                    partialFinders[j].Detect(symbols[i]);
                }
            }
        }

        public void Reset()
        {
            Array.Clear(register, 0, FinderConsts.FrameLength);
            //clear all structures


            for (int i = 0; i < 4; i++)
            {
                partialFinders[i].Clear();
            }

            lastFinderId = -1;
            lastPosition = 0;
        }

        private byte[] Invert(byte[] source)
        {
            int len = source.Length;
            byte[] destination = new byte[len];

            for (int i = 0; i < len; i++)
            {
                destination[i] = (byte)(source[i] ^ 0xFF);
            }

            return destination;
        }
    }
}

﻿/*
 * microp11 2019
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 */

using System;
using System.Diagnostics;

namespace FrameFinder
{
    public class PartialFinder
    {
        public int Tolerance;

        private int finderId;
        private readonly int startRegisterPosition;
        private int normalUWErrors;
        private int invertedUWErrors;
        private bool isNormalPolarity;
        private byte[] partialRegister;
        private long aSC; //absolute symbol count
        private long firstSymbol; //absolute location of the first symbol of the detected partial
        private long lastSymbol;
        private readonly int partialFrameLength;
        private readonly byte[] partialPattern;
        private readonly byte[] partialPatternInverted;
        private int patternPos;
        private bool isDetected;

        public PartialFinder(int finderId, int startRegisterPosition, byte[] partialPattern, byte[] partialPatternInverted, int partialFrameLength)
        {
            aSC = -1;
            Tolerance = 2;
            this.finderId = finderId;
            this.startRegisterPosition = startRegisterPosition;
            this.partialFrameLength = partialFrameLength;
            partialRegister = new byte[this.partialFrameLength];

            int partialPatternlen = partialPattern.Length;
            this.partialPattern = new byte[partialPatternlen];
            Array.Copy(partialPattern, this.partialPattern, partialPatternlen);

            this.partialPatternInverted = new byte[partialPatternlen];
            Array.Copy(partialPatternInverted, this.partialPatternInverted, partialPatternlen);
        }

        public event EventHandler<PartialFinderArgs> OnPartialFind;

        public void Detect(byte symbol)
        {
            aSC++;

            //rotate to left
            Array.Copy(partialRegister, 1, partialRegister, 0, partialRegister.Length - 1);
            //add new symbol
            partialRegister[partialRegister.Length - 1] = symbol;

            //Debug.WriteLine("PartialFinder[{0}] aSC {1}, firstSymbol {2} lastSymbol {3}", finderId, aSC, firstSymbol, lastSymbol);

            //detect
            normalUWErrors = 0;
            invertedUWErrors = 0;
            patternPos = 0;

            for (int symbolPos = 0; symbolPos < partialFrameLength; symbolPos += 162)
            {
                //compute normal polarity
                normalUWErrors += partialPattern[patternPos] ^ partialRegister[symbolPos];
                normalUWErrors += partialPattern[patternPos] ^ partialRegister[symbolPos + 1];

                //compute inverted polarity
                invertedUWErrors += partialPatternInverted[patternPos] ^ partialRegister[symbolPos];
                invertedUWErrors += partialPatternInverted[patternPos] ^ partialRegister[symbolPos + 1];

                patternPos++;
            }

            isDetected = normalUWErrors <= Tolerance || invertedUWErrors <= Tolerance;
            isNormalPolarity = normalUWErrors < invertedUWErrors;
            if (isDetected)
            {
                firstSymbol = aSC - partialFrameLength + 1;
                lastSymbol = aSC;

                byte[] pR = new byte[partialFrameLength];
                Array.Copy(partialRegister, pR, partialFrameLength);
                PartialFinderArgs args = new PartialFinderArgs
                {
                    FinderId = finderId,
                    FirstSymbol = firstSymbol,
                    LastSymbol = lastSymbol,
                    PartialFrameLength = partialFrameLength,
                    PartialRegister = pR,
                    StartRegisterPosition = startRegisterPosition,
                    NormalUWErrors = normalUWErrors,
                    InvertedUWErrors = invertedUWErrors,
                    IsNormalPolarity = isNormalPolarity,
                    ASC = aSC
                };

                OnPartialFind?.Invoke(this, args);

                //clear the partialRegister, superfluous and might be removed after testing
                Array.Clear(partialRegister, 0, partialFrameLength);
            }
        }

        public void Clear()
        {
            aSC = -1;
        }
    }
}

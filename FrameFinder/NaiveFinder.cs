﻿/*
 * microp11 2019
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 */
 
using System;

namespace FrameFinder
{
    public class NaiveFinder: IFinder
    {
        private readonly byte[] register;
        private int nUWErrors;
        private int iUWErrors;
        private int patternPos;
        private bool isDetected;
        private long position;
        private int lastPosition;

        public NaiveFinder()
        {
            register = new byte[FinderConsts.FrameLength];
            position = 0;
            Tolerance = 7;
        }

        public int Tolerance { get; set; }

        public event EventHandler<FrameArgs> OnFrameDetected;

        public void Detect(byte[] symbols, int count)
        {
            for (int i = 0; i < count; i++)
            {
                //rotate to left
                Array.Copy(register, 1, register, 0, FinderConsts.FrameLength - 1);
                //add new symbol
                register[FinderConsts.FrameLength - 1] = symbols[i];
                position++;
                lastPosition++;

                //detect
                nUWErrors = 0;
                iUWErrors = 0;
                patternPos = 0;

                for (int symbolPos = 0; symbolPos < FinderConsts.FrameLength; symbolPos += 162)
                {
                    //compute normal polarity
                    nUWErrors += FinderConsts.Pattern[patternPos] ^ register[symbolPos];
                    nUWErrors += FinderConsts.Pattern[patternPos] ^ register[symbolPos + 1];

                    //compute inverted polarity
                    iUWErrors += FinderConsts.PatternInverted[patternPos] ^ register[symbolPos];
                    iUWErrors += FinderConsts.PatternInverted[patternPos] ^ register[symbolPos + 1];

                    patternPos++;
                }

                isDetected = nUWErrors <= Tolerance || iUWErrors <= Tolerance;
                if (isDetected)
                {
                    FrameArgs e = new FrameArgs
                    {
                        UnmatchedSymbols = lastPosition - FinderConsts.FrameLength,
                        StartPosition = position - FinderConsts.FrameLength,
                        NormalUWErrors = nUWErrors,
                        InvertedUWErrors = iUWErrors,
                        IsNormalPolarity = nUWErrors < iUWErrors,
                        IsElastic = false
                    };
                    OnFrameDetected?.Invoke(this, e);

                    lastPosition = 0;
                }
            }
        }

        public void Reset()
        {
            Array.Clear(register, 0, FinderConsts.FrameLength);
            lastPosition = 0;
            position = 0;
        }
    }
}

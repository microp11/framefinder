﻿/*
 * microp11 2019
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 */
 
using System;

namespace FrameFinder
{
    /// <summary>
    /// This is the argument for the onFrameDetected event.
    /// Add any other properties you see fit.
    /// </summary>
    public class FrameArgs
    {
        /// <summary>
        /// Indicates how many symbols were lost while detecting this frame.
        /// It is an ambigous term, needs more work.
        /// </summary>
        public int UnmatchedSymbols { get; set; }

        /// <summary>
        /// Indicates the position in the stream of received symbols where the
        /// frame starts, inclusive.
        /// </summary>
        public long StartPosition { get; set; }

        /// <summary>
        /// Indicates if the matching was based on the pattern or the inverted
        /// pattern.
        /// </summary>
        public bool IsNormalPolarity { get; set; }

        /// <summary>
        /// Indicates the algorithm needed employ elastic matching on top of the
        /// exact matching.
        /// </summary>
        public bool IsElastic { get; set; }

        /// <summary>
        /// The number of detected normal polarity unique word errors.
        /// </summary>
        public int NormalUWErrors { get; set; }

        /// <summary>
        /// The number of inverse polarity unique word errors.
        /// </summary>
        public int InvertedUWErrors { get; set; }
    }

    public interface IFinder
    {
        /// <summary>
        /// Indicates the max number of pattern errors for either pattern exact
        /// matching or for inverted pattern exact matching.
        /// </summary>
        int Tolerance { get; set; }

        /// <summary>
        /// If frame is detected, raise this event passing the FrameArgs as parameter.
        /// </summary>
        event EventHandler<FrameArgs> OnFrameDetected;

        /// <summary>
        /// Detect method. Will implement your choice algorithm for finding frames.
        /// The symbols will arrive in the same order as they are detected by the
        /// demodulator.
        /// 
        /// Every frame contains 10368 symbols, 128 symbols are the pattern and 10240 are data.
        /// The frame looks like this:
        /// Frame 0                                                                                                                 Frame 1
        /// Pattern[0]|pattern[0]|160-symbols data|Pattern[1]Pattern[1]160-symbols data|....|Pattern[63]Pattern[63]160-symbols data|Pattern[0]|Pattern[0]|...
        /// </summary>
        /// <param name="symbols"></param>
        void Detect(byte[] symbols, int count);

        /// <summary>
        /// Reset internal states such that the detection can start from scratch
        /// at any moment in time.
        /// </summary>
        void Reset();
    }

    public static class FinderConsts
    {
        /// <summary>
        /// Pattern for exact matching.
        /// </summary>
        public static byte[] Pattern =
        {
            0, 0, 0, 0,    0, 1, 1, 1,   1, 1, 1, 0,   1, 0, 1, 0,
            1, 1, 0, 0,    1, 1, 0, 1,   1, 1, 0, 1,   1, 0, 1, 0,
            0, 1, 0, 0,    1, 1, 1, 0,   0, 0, 1, 0,   1, 1, 1, 1,
            0, 0, 1, 0,    1, 0, 0, 0,   1, 1, 0, 0,   0, 0, 1, 0
        };

        /// <summary>
        /// Inverted pattern for exact matching.
        /// </summary>
        public static byte[] PatternInverted =
        {
            1, 1, 1, 1,    1, 0, 0, 0,   0, 0, 0, 1,   0, 1, 0, 1,
            0, 0, 1, 1,    0, 0, 1, 0,   0, 0, 1, 0,   0, 1, 0, 1,
            1, 0, 1, 1,    0, 0, 0, 1,   1, 1, 0, 1,   0, 0, 0, 0,
            1, 1, 0, 1,    0, 1, 1, 1,   0, 0, 1, 1,   1, 1, 0, 1
        };

        /// <summary>
        /// The frame length.
        /// </summary>
        public static int FrameLength = 10368;
    }
}
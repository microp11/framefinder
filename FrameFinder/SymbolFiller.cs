﻿/*
 * microp11 2019
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * This code uses 4 partial finders. The number was selected based on
 * the given 64 UW table.
 * 
 * Based on 16 UW the maximum admisible tolerance is 1 per part. 
 * 
 */

using System;
using System.Diagnostics;

namespace FrameFinder
{
    internal class SymbolFiller
    {
        private readonly int partialFrameLength;
        private byte[] fillerRegister;
        private int fillerLength = 30000;
        private long fillerLeft;
        private long fillerRight;

        public SymbolFiller(int partialFrameLength)
        {
            this.partialFrameLength = partialFrameLength;
            fillerRegister = new byte[fillerLength];
            fillerLeft = -fillerLength;
            fillerRight = -1;
        }

        internal void Store(byte symbol)
        {
            //rotate to left
            Array.Copy(fillerRegister, 1, fillerRegister, 0, fillerLength - 1);
            //add new symbol
            fillerRegister[fillerLength - 1] = symbol;

            //absolute positions
            fillerLeft++;
            fillerRight++;

            //Debug.WriteLine("fillerLeft {0}, fillerRight {1}, symbol {2}", fillerLeft, fillerRight, symbol);
        }

        internal PartialFinderArgs GetPart(int id, long from, long to, bool x = false)
        {
            PartialFinderArgs args = new PartialFinderArgs
            {
                FinderId = id,
                FirstSymbol = from,
                LastSymbol = to,
                PartialRegister = new byte[partialFrameLength]
            };

            //compute data
            long toDistanceFromRight, fromDistanceFromRight;
            long toFillerDistanceFromRight, fromFillerDistanceFromRight;

            fromDistanceFromRight = fillerRight - from - 1;
            fromFillerDistanceFromRight = 29999 - fromDistanceFromRight - 1;

            try
            {
                if (x)
                {
                    //Debug.WriteLine("FROM {0}:{1}", fromFillerDistanceFromRight + 2, fillerRegister[fromFillerDistanceFromRight + 1]);
                    Debug.WriteLine("{0} FROM {1}:{2} (absolute {3})", id, fromFillerDistanceFromRight + 1, fillerRegister[fromFillerDistanceFromRight + 0], from);
                    //Debug.WriteLine("FROM {0}:{1}", fromFillerDistanceFromRight + 0, fillerRegister[fromFillerDistanceFromRight - 1]);
                }

                toDistanceFromRight = fillerRight - to;
                toFillerDistanceFromRight = 29999 - toDistanceFromRight;

                if (x)
                {
                    //Debug.WriteLine("TO {0}:{1}", toFillerDistanceFromRight + 2, fillerRegister[toFillerDistanceFromRight + 1]);
                    Debug.WriteLine("{0} TO   {1}:{2} (absolute {3})", id, toFillerDistanceFromRight + 1, fillerRegister[toFillerDistanceFromRight + 0], to);
                    //Debug.WriteLine("TO {0}:{1}", toFillerDistanceFromRight + 0, fillerRegister[toFillerDistanceFromRight - 1]);
                }

                Array.Copy(fillerRegister, fromFillerDistanceFromRight, args.PartialRegister, 0, partialFrameLength);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NON-CONTIGUOUS exception! Looks like a frame but it is an interrupted stream matched to a frame.");
            }
            return args;
        }

        internal void SetSymbol(int v)
        {
            fillerRegister[29999] = (byte)v;
        }
    }
}
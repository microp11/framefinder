﻿/*
 * microp11 2019
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 */
 
namespace FrameFinder
{
    public class PartialFinderArgs
    {
        public int FinderId { get; set; }
        public long FirstSymbol { get; set; }
        public long LastSymbol { get; set; }
        public int PartialFrameLength { get; set; }
        public byte[] PartialRegister { get; set; }
        public int StartRegisterPosition { get; set; }
        public int NormalUWErrors { get; set; }
        public int InvertedUWErrors { get; set; }
        public bool IsNormalPolarity { get; set; }
        public long ASC { get; set; }
    }
}
